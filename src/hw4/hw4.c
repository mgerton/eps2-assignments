//	Matt Gerton
//	February 11, 2007	Homework 4
//	Section B38
//	Description: simulate an ATM machine while implementing the use of functions

#include <stdio.h>

float withdrawal(float bal);
float deposit(float bal);
float inquiry(float bal);
void printMenu(void);


int main()
{
	int choice = 0;
	float balance = 100.00;


	printf("=====================HAWKEYE BANK=====================\n");
	printf("Thank you for choosing to open a bank account with us!\n");
	printf("You currently have $%.2f in your account.\n", balance);

	while(choice != 4)
	{
		
		printMenu();		//	print the menu
		printf("Enter your choice: ");
		scanf("%d", &choice);
	

		switch (choice)
		{
			case 1:	balance = deposit(balance);		//	call the deposit function
					break;

			case 2:	balance = withdrawal(balance);	//	call the withdraw function
					break;


			case 3:	printf("\n\nCurrent balance: $%.2f\n\n", inquiry(balance));		//	inquiry
					break;
		
			case 4:	break;		//	exit

			default: printf("\n\nERROR - You've entered your option incorrectly.\n\n");
		}
	}


	return 0;
}

float withdrawal(float bal)			//	withdraw function
{
	float y = 0.00, total = 0.00;
	printf("\nPlease enter the amount you want to withdraw: ");
	scanf("%f", &y);															//	deposit
	if (y < 0.00)
	{
		printf("ERROR - Sorry, you cannot withdraw a negative amount.\n\n");		//	error
		printf("\nPlease enter the amount you want to withdraw: ");					//	check
		scanf("%f", &y);
		printf("%.2f", y);
	}
	else if (y > bal)
	{
		printf("ERROR - Sorry, you cannot withdraw more that what you have in your account.\n");
		printf("\nPlease enter the amount you want to withdraw: ");
		scanf("%f", &y);
		printf("%.2f", y);
	}
	total = bal - y;
	printf("\nA withdrawal of $%.2f has been taken out of your account.", y);
	printf("\n\nYour current balance is $%.2f\n\n", total);
	
	return total;
}

float deposit(float bal)			//	deposit function
{
	float x = 0.00, total = 0.00;
	printf("\nPlease enter the amount you want to deposit: ");
	scanf("%f", &x);															//	deposit
	if (x < 0.00)
	{
		printf("ERROR - Sorry, you cannot deposit a negative amount.\n\n");		//	error
		printf("\nPlease enter the amount you want to deposit: ");				//	check
		scanf("%f", &x);
		printf("%.2f", x);
	}
	total = bal + x;
	printf("\nA deposit of $%.2f has been made into your account.", x);
	printf("\n\nYour current balance is $%.2f\n\n", total);
	
	return total;
}

float inquiry(float bal)						//	inquiry function
{
	return bal;
}

void printMenu(void)
{
	printf("\n 1.........Deposit\n");
	printf(" 2.........Withdrawal\n");										//	prints the menu
	printf(" 3.........Inquiry\n");
	printf(" 4.........Exit\n");
}