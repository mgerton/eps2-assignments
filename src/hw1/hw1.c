//	Matt Gerton
//	January 22, 2006	Homework 1
//	Description: Prints the name, section, and hometown of the programmer

#include <stdio.h>

int main(void)
{
	printf("Matthew Gerton\n");
	printf("Section B38\n");
	printf("Mundelein, IL\n");

	return 0;
}