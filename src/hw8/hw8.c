/*	
	Matt Gerton
	March 25, 2007	Homework 8
	Section B38
	Description: a program that calculates loans and displays it in a 
			 	 neat and organized table using formatted input and output
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>

void table(double amt, int pct, int months);
void line(void);
void cap(char *p);
void customer(char *t[]);
void phone(char *ptr);

int main()
{
	char dat[128], firstEntry[128], *token[12];
	int strLen = 128, account, i, months;
	double amount, interest;
	
	printf(" Enter the account number, loan amount, \n");
	printf(" interest rate and loan duration:\n");

	token[0] = sscanf(dat, " ");
	fgets(dat,1500,stdin);
	sscanf(dat, "%s", firstEntry);
	account = atoi(firstEntry);

	printf("The account # is %d\n", account);
	
	for(i = 1;i < 12;i++)
	{
		token[i] = strtok(NULL, " ");
		cap(token[i]);
		if(i == 7)
			cap(token[i]+1);
		printf(" Token number  %2d   '%s'\n", i, token[i]);
	}
	
	amount = atoi(token[9]);
	interest = atoi(token[10]);
	months = atoi(token[11]);

	table(1000,120,24);	//	fake call


	return 0;
}

/*
	line() Function: prints a dotted line across the screen; return type is void.
				   
*/

void line(void)
{
	int x;

	for(x = 0; x <= 76; x++)
	{
		printf("-");
	}
	printf("\n");
}

/*
	table() Function: prints the information received from the main() function
					  in a neatly organized table.
*/

void table(double amt, int pct, int months)
{
	int i;
	double pmt, iPm, princip, exp, ATI, RP, ATP;
	
	line();

	printf(" Loan Amount:  $%.2lf    APR:  %d.00%%		Period:  %d\n", amt, pct, months);
	
	line();

	printf("	Pay #	Payment		Principal Paid		Interest Paid	Remaining Balance\n");
	line();

	iPm = pct/(100.0 * 12);
	exp = months;
	pmt = amt * (iPm * pow((1.0 + iPm),exp))/(pow((1.0 + iPm),exp) - 1.0);
	princip = amt;
	RP = amt;

	for(i = 0; i <= months; i++)
	{
		ATI = princip * iPm;
		ATP = pmt - ATI;
		RP = RP - ATP;
		printf("  %d\t%.2lf\t%.2lf\t%.2lf\t%.2lf\n",i,pmt,ATP,ATI,RP);

		princip = princip - ATP;
	}

	line();

	return;
}

/*
	cap() Function: changes lowercase letters to upppercase letters

*/
void cap(char *p)
{
	if(islower(*p))
		*p = toupper(*p);
	return;
}

/*
	customer() Function: receives a pointer and separates the customer data
						 from the rest of the data.
*/
void customer(char *t[])
{
	printf("customer #: %.10d\n",atoi(t[0]));
	printf("%s	%s\n",t[1],t[2]);
	phone(t[8]);

	return;
}

/*
	table() Function: receives the data from the main() function and separates the
					  phone number data from the rest of the dataand formats it.
*/
void phone(char *ptr)
{
	char temp[4] = {0};

	strncpy(temp,ptr,3);
	printf("(%5)",temp);

	strncpy(temp,ptr+3,3);
	printf("%5-",temp);
	printf("%5\n",ptr+6);

	return;
}