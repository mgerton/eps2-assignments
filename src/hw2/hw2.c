//	Matt Gerton
//	January 30, 2007	Homework 2
//	Description: Asks the user for integer values and checks whether they are even or odd

/* ********************Pseudocode******************** */

/*
	Initialize variables
	ask user for number of values to be entered

  	while the input does not equal the the counter
		user enters a value
		check to see if number is even or odd
			if number is even
				display number is even
			else
				display number is odd
		counter is incremented
	exit loop

	end program
*/

#include <stdio.h>

int main(void)
{
	int counter = 0, input = 0, x = 0;

	printf("Enter the number of integer values to be entered: ");
	scanf("%d", &x);
	
	while(counter != x)
	{
		printf("\nPlease enter a value: ");
		scanf("%d", &input);
		if (input % 2 == 0)
			printf("\nThe number is even.\n");
		else
			printf("\nThe number is odd.\n");
		
		counter = counter + 1;
	}


	return 0;
}