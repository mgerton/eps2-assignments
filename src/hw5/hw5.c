//	Matt Gerton
//	February 20, 2007	Homework 5
//	Section B38
//	Description: a simple tic-tac-toe game using arrays
//		portions of code based off of tic-tac-toe example posted on ICON

#include <stdio.h>

void printBoard(char a[3][3]);

int main() {
	int row = 0, col = 0, z = 0;
	char a[3][3] = {' '};

	while(z < 9) {
		printBoard(a);

		if (z % 2 == 0) {
			printf("Player X's move:\n\n");
			printf("Row: ");
			scanf("%d", &row);
			printf("Col: ");
			scanf("%d", &col);
			a[row][col] = 'X';
		}

		if (z % 2 == 1) {
			printf("Player O's move:\n\n");
			printf("Row: ");
			scanf("%d", &row);
			printf("Col: ");
			scanf("%d", &col);
			a[row][col] = 'O';
		}

		z++;
	}
}

//	function for printing the tic-tac-toe board
void printBoard(char board[3][3]) {
   	int x, y;

	printf("\n");

   	for(x = 0; x < 3; x++) {
		if(x != 0)
			printf("\n---+---+---\n");	//  print the lines between the rows

		for(y = 0; y < 3; y++)  {
			if (y != 0)
				printf(" |");	//	the vertical lines on the board
	        if (board[x][y] == 'X')
				printf(" X");	//	print the X
        	else if (board[x][y] == 'O')
				printf(" O");	//	print the O
 			else
				printf("  ");
      	}
   }

   printf("\n\n");
}
