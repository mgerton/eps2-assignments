//	Matt Gerton
//	February 20, 2007	Homework 5
//	Section B38
//	Description: a simple tic-tac-toe game using arrays
//		portions of code based off of tic-tac-toe example posted on ICON

#include <stdio.h>
#include <stdbool.h>

typedef struct player player;
typedef struct board board;

// defining a bool type with enums
// typedef enum { false, true } bool;

void print_board(struct board* board);
void make_move(struct player* current_player, struct board* board);

struct player {
	char character;
	int current_move[2];
	bool is_winner;
};

struct board {
	int rows;
	int cols;
	char current_state[][];
};

void print_board(struct board *board) {
	int r;
	int c;
	int *rows = &board->rows;
	int *cols = &board->cols;

	char x = 'X';
	char o = 'O';

	printf("\n");

	for (r = 0; r < *rows; r++) {
		if (r != 0) {
			printf("---+---+---\n");
		}

		for (c = 0; c < *cols; c++) {
			if (c != 0) {
				printf(" |");
			}

			if (&board[r][c] == x) {
				printf(" %d", x);
			} else if (&board[r][c] == o) {
				printf(" %d", o);
			} else {
				printf("  ");
			}
		}
	}

	printf("\n\n");
}

void make_move(struct player *current_player, struct board *board) {
	int *rows = &board->rows;
	int *cols = &board->cols;
	int choice_row = 0;
	int choice_col = 0;

	char board_array[*cols][*rows];
	char *character = &current_player->character;

	printf("Player %d's move:\n\n", *character);
	printf("Row: ");
	scanf("%d", &choice_row);
	printf("Col: ");
	scanf("%d", &choice_col);

	&board->current_state[]
	board_array[choice_row][choice_col] = *character;
}

int main() {
	board board;
	board.rows = 3;
	board.cols = 3;

	// we always start with an empty board
	int spaces_left = board.rows * board.cols;

	// more game flow vars
	bool is_game_over = false;
	int player_order = 0;

	player player_x;
	player_x.character = 'X';

	player player_o;
	player_o.character = 'O';

	player* current_player;

	while (!is_game_over) {
		if (spaces_left == 0) {
			is_game_over = true;
		}

		print_board(&board);

		if (player_order % 2 == 0) {
			current_player = &player_x;
		}

		if (player_order % 2 == 1) {
			current_player = &player_o;
		}

		// make dat move, yo
		make_move(current_player, &board);
		spaces_left--;
		player_order++;
	}
}
