//	Matt Gerton
//	March 4, 2007	Homework 6
//	Section B38
//	Description: a program which takes input and encodes or decodes it while
//		implementing arrays and pointers

#include <stdio.h>

void encode(char alp[ ], char cip[ ], char msg[ ]);
void decode(char alp[ ], char cip[ ], char msg[ ]);

int main()
{
	int choice = 0;
	
	char stuff;
	char message[500] = {' '};
	char alphabet[26] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
						 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

	char cipher[26]  =  {'m', 'a', 't', 'u', 'g', 'e', 'r', 'v', 'o', 'n', 'b', 'y', 'x',
						 'h', 'l', 's', 'd', 'w', 'f', 'z', 'i', 'p', 'k', 'q', 'j', 'c'};
	
	printf("Message Encoder and Decoder v1.0\n");
	printf("================================\n");
	printf("1. Encode a message\n");
	printf("2. Decode a message\n");
	printf("3. Exit program\n\n");
	printf("Enter the option you wish to perform: ");
	scanf("%d", &choice);
	scanf("%c", &stuff);

	
	while(choice != 3)
	{
	
		switch (choice)
		{
			case 1:	printf("Enter the message you wish to encode: ");
					fgets(message, 500, stdin);
					encode(message, alphabet, cipher);
					break;

			case 2:	printf("Enter the message you wish to decode: ");
					fgets(message, 500, stdin);
					decode(message, cipher, alphabet);
					break;


			case 3:	break;

			default: printf("ERROR - Invalid option. Please try again.");
	
		  }
	}
	return 0;
}

void encode(char alp[ ], char cip[ ], char msg[ ])
{
	int alpIndex, msgIndex = 0;
	
	while(msg[msgIndex] != '\0')
	{
		for(alpIndex = 0; alpIndex < 27; alpIndex++)
		{
			if(msg[msgIndex] == alp[alpIndex])
				break;
		}
		if(alpIndex < 26)
			printf("%c", cip[alpIndex]);
		else
			printf("%c", msg[msgIndex]);

		msgIndex++;
	}
}

void decode(char alp[ ], char cip[ ], char msg[ ])
{
	int counter = 0;
	
	char *ptrAlp = alp, *ptrMsg = msg, *ptrCip = cip;

	
	while(*ptrMsg != 0)
	{
		for(counter = 0; counter < 26; counter++)
		{
			if(*ptrMsg == *(ptrCip + counter))
				break;
			if(counter < 26)
				printf("%c", *(ptrAlp + counter));
			else
				printf("%c", *ptrMsg);

			ptrMsg++;
		}
	}
}
