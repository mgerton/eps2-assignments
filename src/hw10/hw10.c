/*	
	Matt Gerton
	April 17, 2007	Homework 10
	Section B38
	Description: reads genetic codes from an outside file (file i/o) 
			 	 and displays the output using MATLab
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct gene					//	gene structure
{
  char accession[50];
  char geneName[50];
  char sequence[1000];
};

int findVertBar(char string[]);
float gcCount(char* a, int window);

int main(void)
{
	FILE *fp;
	struct gene Gene[5];
	int i = 0;
	char line[200];
	char temp[200];
	int pos = 0;
	
	for(i = 0; i < 4; i++)
	{
	  strcpy(Gene[i].accession, "");
	  strcpy(Gene[i].geneName, "");
	  strcpy(Gene[i].sequence, "");
	}

	if((fp = fopen("input.fa", "r")) == NULL)
	{
		printf("Failed to open input.fa\n");
		exit(1);
	}

	i = 0;
	fgets(line, 200, fp);
	while(!feof(fp))
	{
		strcpy(temp, line);
		strcpy(line, &temp[1]);  // this drops the ">" at beginning of line 
		strcpy(temp, line);

		pos = findVertBar(line);
		if(pos >= 0)
		{
			strncpy(Gene[i].accession, line, pos);
			Gene[i].accession[pos] ='\0'; // make sure my string has /0 at end
			printf("accession = %s\n", Gene[i].accession);    
			strcpy(line, &temp[pos + 1]);  // this drops the accession number and "|" at beggining of line
			strcpy(temp, line); 
		}   
		else
		{
			printf("Error -- could not find '|'\n");
			exit(1);
		}

		pos = findVertBar(line);
		if(pos >= 0)
		{
			strncpy(Gene[i].geneName, line, pos);
			Gene[i].geneName[pos] ='\0';
			printf("name = %s\n", Gene[i].geneName);    
			strcpy(line, &temp[pos + 1]);  // drops the gene name and "|" from the beginning of the line
			strcpy(temp, line); 
		}   
		else
		{
			printf("Error -- could not find '|'\n");
			exit(1);
		}

		fgets(temp, 200, fp); 
		do
		{ 
			temp[strlen(temp) - 1] ='\0';  // convert new-line to string terminator
			strcat(Gene[i].sequence, temp); 
			fgets(temp, 200, fp); 
		}while((temp[0] != '>') && (!feof(fp)));

		printf("sequence = '%s'\n", Gene[i].sequence);    
		strcpy(line, temp);
		i++;
	}

	gcCount(line, window);

}

/*
	findVertBar() Function: receives a string and then finds the position of a "|"
							in the given string.
				   
*/
int findVertBar(char string[])
{
	int i;

	for(i = 0; i < strlen(string); i++)
	{
		if(string[i] == '|')
		
		return(i);
	}

	return(-1);
}

/*
	gcCount() Function: searches the string for "G"'s and "C"'s
				   
*/
float gcCount(char* a, int window)
{
	int i;
	int count = 0;

	for(i = -1 * window/2; i<window/2; i++)
	{
		if(*(a+i) == 'G' || *(a+i) == 'C')
		{
			count++;
		}
	}

	return ((float)count)/((float)window);
}
