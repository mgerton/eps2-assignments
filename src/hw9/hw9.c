/*	
	Matt Gerton
	April 2, 2007	Homework 9
	Section B38
	Description: a program that simulates an address book while implementing 
			 	 structures
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#define MAX_count 3

int inputEntry(Entry addr[], int z);
void printEntry(BookEntry addr[], int x);
int errorCheck(char t[]);
int menu(void);

typedef struct name				//	name structure
{
	char first[21];
	char last[21];
	char title[15];
}Name;

typedef struct address			//	address structure
{
	char num[10];
	char stName[25];
	char abbrev[10];
	char city[30];
	char state[25];
	int zipCode;
}Address;

typedef struct entry			//	entry structure
{
	Name person;
	Address home;
	int age;
}BookEntry;

int main()
{
	int selection, i, numBook = 0;
	BookEntry book[3];

	do
	{
		selection = menu();
		switch(selection)
		{
			case 1:
				numBook = inputEntry(book, numBook);
				break;
			case 2:
				if(numBook == 0)
					printf("There are currently no entries.");
				else
					for(i = 0; i < numBook; i++)
					{
						printEntry(book, i);
					}
					break;
			case 3:
				printf("\nThank you for using the interactive address book. Goodbye.");
				break;
			default:
				printf("ERROR - your option is invalid. Please choose another.");
				break;
		}
	}while(selection != 3);


	return 0;
}

/*
	menu() Function: prints the menu and returns the user's choice as an integer.
				   
*/

int menu(void)
{
	int choice;
	char stuff;

	printf("Interactive Address Book v1.0\n");
	printf("      By: Matt Gerton %c\n", 016);
	printf("=============================\n");
	printf("1. Add an entry\n");
	printf("2. Print all entries\n");
	printf("3. Exit\n");
	printf("Enter choice: \n");

	scanf("%d", &choice);
	scanf("%c", &stuff);

	return choice;
}

/*
	inputEntry() Function: asks for user input (name, address, etc.)
*/

int inputEntry(Entry addr[], int z)
{
	char field[10][21], input[500];
	int check = 0;
	
	if(count >= 3)
	{
		printf("Error");

		return count;
	}

	do
	{
		printf("Enter Title and Name: ");
		fgets(input, 500, stdin);
		sscanf(input, %s%s%s, field[0], field[1], field[2]);
		check = errorCheck(field[0]);
		check = check + errorCheck(field[1]);
		check = check + errorCheck(field[2]);
	}while(check < 3);

	do
	{
		printf("Enter Address, City, State, and zip: ");
		fgets(input, 500, stdin);
		sscanf(input, %s%s%s%s%s%s, field[3], field[4], field[5], field[6], field[7], field[8], field[9]);
		check = errorCheck(field[3]);
		check = check + errorCheck(field[4]);
		check = check + errorCheck(field[5]);
		check = check + errorCheck(field[6]);
		check = check + errorCheck(field[7]);
		check = check + errorCheck(field[8]);
		check = check + errorCheck(field[9]);
	}while(check < 6);

	do
	{
		printf("Enter Age: ");
		fgets(input, 500, stdin);
		sscanf(input, %d, field[10]);
		check = errorCheck(field[10]);
	}while(check < 1);

	char *strcpy(addr[count].person.title, field[0])
	char *strcpy(addr[count].person.first, field[1])
	char *strcpy(addr[count].person.last, field[2])
	char *strcpy(addr[count].home.num, field[3])
	char *strcpy(addr[count].home.stName, field[4])
	char *strcpy(addr[count].home.abbrev, field[5])
	char *strcpy(addr[count].home.city, field[6])
	char *strcpy(addr[count].home.state, field[7])
	*addr[count].home.zipCode = atoi(field[8])
	*addr[count].home.age = atoi(field[9])
}

/*
	printEntry() Function: prints the entries listed in the address book
*/
void printEntry(Entry addr[], int x)
{
	printf("\nName:  %  9s  %s  %s  %s  \n","", addr[x].person.title, addr[x].person.first, addr[x].person.last);
	printf("Address:  %5s  %s  %s  %s  \n","",addr[i].home.num, addr[i].home.stName, addr[i].home.abbrev);
	printf("%   16s   %s   %s, %.5d      \n","",addr[i].home.city, addr[i].home.state, addr[i].home.zipCode);
	printf("Age:  %10s","", addr[i].age);
}

/*
	customer() Function: receives a pointer and separates the customer data
						 from the rest of the data.
*/
int errorCheck(char t[])
{
	int checker = 1;

	return checker;
}