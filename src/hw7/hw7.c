/*	
	Matt Gerton
	March 19, 2007	Homework 7
	Section B38
	Description: a program that does the quadratic formula while 
			 	 implementing pointers and pass-by-reference functions
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>

void factors(int *a, int *b, int *c, double *rootOne, double *rootTwo);
void read(int *x, int *y, int *z);
void getCoef(int *ptr);

int main()
{
	int a = 0, b = 0, c = 0, *A, *B, *C;
	double rtOne = 0.00, rtTwo = 0.00, *RtOne, *RtTwo;
	A = &a;
	B = &b;
	C = &c;
	RtOne = &rtOne;
	RtTwo = &rtTwo;

	printf("QUADRATIC EQUATION SOLVER v1.0\n\n");;
	printf("Copyright 2007 University of Iowa\n");
	printf("             College of Engineering\n\n");
	printf("By: Matt Gerton %c\n", 016);				//	just so you know, i added the ASCII character on purpse
	printf("===================================\n\n");	//	just for style :-)
	
	read(A, B, C);

	factors(A, B, C, RtOne, RtTwo);

}

/*
	read() Function: calls the getCoef() function and displays the contents of
				   the integer pointers.
*/

void read(int *x, int *y, int *z)
{
	getCoef(x);
	getCoef(y);
	getCoef(z);
}

/*
	factors() Function: passes pointers to ints and doubles and uses them to
						calculate the factors of the given coefficients.
*/

void factors(int *a, int *b, int *c, double *rootOne, double *rootTwo)
{
	double disc;

	disc = (*b) * (*b) - 4 * (*a) * (*c);		//	top half of quadratic equation

	if (disc >= 0)
	{
		disc = sqrt(disc);
		*rootOne = (-(*b) + disc)/(2 * (*a));	//	bottom half of quadratic equation
		*rootTwo = (-(*b) - disc)/(2 * (*a));

		printf("The roots are real:\n\n\t%lf\n and \n\t%lf\n\n", *rootOne, *rootTwo);	//	displays the roots
	}
	else
	{
		printf("The roots are a complex conjugate pair:\n");		//	displays the roots in an easier to read format
		printf("\n\t%d + %c%.01f", -(*b), 251, disc);
		printf("\n\t-----------\n\t     %d\n     & \n", 2 * (*a));
		
		printf("\n\t%d - %c%.01f", -(*b), 251, disc);
		printf("\n\t-----------\n\t     %d\n\n", 2 * (*a));
	}
}

/*
	getCoef() Function: is passed a pointer and reads and interprets the input, telling
						the user whether or not his/her input is valid.
*/

void getCoef(int *ptr)
{
	char buffer[30];
	int digit, i, coef;

	do
	{
		printf("Enter integer: ");
		gets(buffer);
		digit = 1;
		for(i=0; buffer[i] != '\0'; i++)
		{
			if(!isdigit(buffer[i]))
			{
				digit = 0;
				break;
			}
		}
		if(digit == 0)		//	error-checking
		{
			printf("ERROR - %s not an integer\n\n", buffer);
		}
		else
		{
			coef = atoi(buffer);
		}
	}
	while(digit == 0 || coef == 0);
	*ptr = coef;

	return;
}