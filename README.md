## EPS 2 Assignments

A collection of old C source files from college, up here for nostalgia's sake. These were all done in the Spring semester of 2007 at the [University of Iowa](http://www.uiowa.edu), in the College of Engineering's course ["Engineering Problem Solving II"](https://isis.uiowa.edu/isis2/courses/details.page?_ticket=GaMBXyh6VwGvEgcOl_ZbWvfv4JnuILYM&id=785501&ci=155147).